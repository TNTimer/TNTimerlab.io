var people = {}

people.formatTime = function(timeIn) {
    var outTime = ''
    if(timeIn > 3600) {
        outTime += Math.floor(timeIn/3600) + ':'
    }
    if(timeIn > 60) {
        outTime += Math.floor(timeIn%3600/60) + ':'
    }
    var tmp = timeIn%60
    
    tmp = tmp.toString().split('.')
    try {
        tmp[1] = tmp[1].slice(0, 3)
    } catch (e) {}
    if(tmp.length === 1) {
        tmp.push('.000')
    }
    if(tmp[0].length === 1) {
        tmp[0] = '0' + tmp[0]
    }
    while (tmp[1].length < 3) {
        tmp[1] += '0'
    }
    return outTime + tmp.join('.')
}

people.displayTime = function(time) {
    let timeElements = document.getElementsByClassName('times')
    for(var i = 0; i < timeElements.length; i++) {
        timeElements[i].innerHTML += '<li>' + time + '</li>'
        timeElements[i].scrollTop = timeElements[i].scrollHeight
    }
}

people.displaySessionDropdown = function() {
    var select = document.getElementById('session')
    select.innerHTML = ''
    for(var key in timer.getAllSessions()) {
        var option       = document.createElement('option')
        option.innerHTML = key
        option.value     = key
        select.appendChild(option)
    }
}

people.displayWarning = function() {
    
}