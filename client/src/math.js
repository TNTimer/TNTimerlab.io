timer.mo3 = function() {
    var ao = timer.getSession(timer.session).slice(-3)
    if(ao.length === 3) {
        var ao = ao.map(function(value) {return Number(value.time)})
        return ao.reduce(function(a, b) {return a + b})/3
    }
}

timer.ao5 = function() {
    var ao = timer.getSession(timer.session).slice(-5)
    if(ao.length === 5) {
        var ao = ao.map(function(value) {return Number(value.time)})
        var max = Math.max(...ao)
        var min = Math.min(...ao)
        ao.splice(ao.indexOf(max), 1)
        ao.splice(ao.indexOf(min), 1)
        return ao.reduce(function(a, b) {return a + b})/3
    }
}

timer.ao12 = function() {
    var ao = timer.getSession(timer.session).slice(-12)
    if(ao.length === 12) {
        var ao = ao.map(function(value) {return Number(value.time)})
        var max = Math.max(...ao)
        var min = Math.min(...ao)
        ao.splice(ao.indexOf(max), 1)
        ao.splice(ao.indexOf(min), 1)
        return ao.reduce(function(a, b) {return a + b})/10
    }
}

timer.ao50 = function() {
    var ao = timer.getSession(timer.session).slice(-50)
    if(ao.length === 50) {
        var ao = ao.map(function(value) {return Number(value.time)})
        var max = Math.max(...ao)
        var min = Math.min(...ao)
        ao.splice(ao.indexOf(max), 1)
        ao.splice(ao.indexOf(min), 1)
        return ao.reduce(function(a, b) {return a + b})/48
    }
}

timer.ao100 = function() {
    var ao = timer.getSession(timer.session).slice(-100)
    if(ao.length === 100) {
        var ao = ao.map(function(value) {return Number(value.time)})
        var max = Math.max(...ao)
        var min = Math.min(...ao)
        ao.splice(ao.indexOf(max), 1)
        ao.splice(ao.indexOf(min), 1)
        return ao.reduce(function(a, b) {return a + b})/98
    }
}