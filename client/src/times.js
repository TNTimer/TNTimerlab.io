// time storage and management
timer.createSession = function(name) {
    if(timer.getAllSessions()[name] == null) {
        timer.setSession(name, [])
    }
    var select     = document.getElementById('session')
    var dropdown   = document.createElement('option')
    dropdown.value = name
    
}

timer.getAllSessions = function() {
    return JSON.parse(localStorage.times)
}

timer.getSession = function(sessionName) {
    return timer.getAllSessions()[sessionName]
}

timer.setSession = function(session, value) {
    var times = timer.getAllSessions()
    times[session] = value
    localStorage.times = JSON.stringify(times)
}

timer.saveTime = function(time, penalties) {
    var times = timer.getAllSessions()
    times[timer.session].push({time: time, timestamp: Date.now(), plusTwo: penalties.plusTwo, DNF: penalties.DNF})
    localStorage.times = JSON.stringify(times)
    timer.reloadUI(time)
}

timer.deleteSession = function(name) {
    if(name !== 'default') {
        var allTimes = timer.getAllSessions()
        delete allTimes[name]
        localStorage.times = JSON.stringify(allTimes)
    } else {
        var allTimes = timer.getAllSessions()
        allTimes.default = []
        localStorage.times = JSON.stringify(allTimes)
    }
}

timer.deleteLatestTime = function() {
    var times = timer.getAllSessions()
    times[timer.session].pop()
    localStorage.times = JSON.stringify(times)
    
    timer.reloadUI()
}

timer.setCurrentSession = function(sessionName) {
    timer.session = sessionName
    timer.reloadUI()
}