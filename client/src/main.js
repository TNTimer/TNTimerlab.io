function addEvent( obj, type, fn ) {
  if ( obj.attachEvent ) {
    obj['e'+type+fn] = fn;
    obj[type+fn] = function(){obj['e'+type+fn]( window.event );}
    obj.attachEvent( 'on'+type, obj[type+fn] );
  } else
    obj.addEventListener( type, fn, false );
}
function removeEvent( obj, type, fn ) {
  if ( obj.detachEvent ) {
    obj.detachEvent( 'on'+type, obj[type+fn] );
    obj[type+fn] = null;
  } else
    obj.removeEventListener( type, fn, false );
}
var timer = {}

timer.init = function() {
    // load preferences
    preferences.init()

    if(!localStorage.times) {
        localStorage.times = JSON.stringify({"default":[]})
    }
    timer.session = "default"
    
    timer.reloadUI()
    
    timer.currentState = timer.timerState.idle()
}

// declare UI states

// timer state
timer.timerState = {}
timer.timerState.idle = function() {
    var stateInfo = {
        state:    'timer',
        subState: 'idle',
        info: {}
    }
    return stateInfo
}

timer.timerState.inspection = function(started) {
    var stateInfo = {
        state:    'timer',
        subState: 'inspection',
        info: {
            startedTimeStamp: started
        }
    }
    return stateInfo
}

timer.timerState.timing = function(started) {
    var stateInfo = {
        state:    'timer',
        subState: 'timing',
        info: {
            startedTimeStamp: started
        }
    }
    return stateInfo
}

timer.reloadUI = function() {
    var timeElements = document.getElementsByClassName('times')
    for(var i = 0; i < timeElements.length; i++) {
        timeElements[i].innerHTML = ""
    }
    
    var times = JSON.parse(window.localStorage.times)[timer.session]
    for(var i = 0; i < times.length; i++) {
        people.displayTime(times[i].time)
    }
    people.displaySessionDropdown()
    
    document.getElementById('session').value = timer.session
}

// state handling
timer.setState = function(state) {
    timer.currentState = state
    try {
        timer.stateChangeEvents.all.map(function(fn){fn(state)})
        timer.stateChangeEvents[state.state][state.subState].map(function(fn){fn(state)})
        timer.stateChangeEvents[state.state].all.map(function(fn){fn(state)})
    } catch (e) {}
}

timer.onStateChange = function(state, subState, callback) {
    if(timer.stateChangeEvents[state] == null) {
        timer.stateChangeEvents[state] = {}
    }
    if(timer.stateChangeEvents[state][subState] == null) {
        timer.stateChangeEvents[state][subState] = []
    }
    timer.stateChangeEvents[state][subState].push(callback)
}

timer.stateChangeEvents = {all: []}


addEvent(window, 'storage', function(event) {
    alert(JSON.stringify(event))
})

window.onerror = function(message, url, lineNumber) {  
  alert('Oops! It appears that you\'ve run into an error.'
    + 'Try reloading the page and trying again.'
    + 'If the problem persists, please report the error.'
  )
};  