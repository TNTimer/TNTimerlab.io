var preferences = {}
preferences.init = function() {
    if(!localStorage.preferences) {
        localStorage.preferences = JSON.stringify({
            useInspection: false,
            theme: 'default',
            hideWhileSolving: false
        })
    }
    preferences.prefs = JSON.parse(localStorage.preferences)
}