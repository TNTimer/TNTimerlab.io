// timer start
timer.spaceDoStart = true
timer.touchDoStart = true

addEvent(window, 'keyup', function(event) {
    if(event.code === 'Space' && timer.currentState.state === 'timer') {
        if(timer.currentState.subState === 'idle' && timer.spaceDoStart) {
            timer.penalties = {}
            if(preferences.prefs.useInspection) {
                timer.setState(timer.timerState.inspection(performance.now()))
            } else {
                timer.setState(timer.timerState.timing(performance.now()))
            }
        } else if (timer.currentState.subState === 'inspection') {
            var oldState = timer.currentState
            timer.setState(timer.timerState.timing(performance.now()))
        }
    }
    // time ops
    if(event.code === "Equal" && event.shiftKey) {
        var times = JSON.parse(localStorage.times)
        var last = times.pop()
        last.plusTwo = true
        times.push(last)
        localStorage.times = JSON.stringify(times)
        timer.reloadUI()
    }
    
    timer.spaceDoStart = true
})

addEvent(document.getElementById('timer'), 'touchend', function(event) {
    if(timer.currentState.state === 'timer' && timer.currentState.subState === 'idle' && timer.touchDoStart) {
        if(preferences.prefs.useInspection) {
            timer.setState(timer.timerState.inspection(performance.now()))
        } else {
            timer.setState(timer.timerState.timing(performance.now()))
        }
    }
    timer.touchDoStart = true
})

timer.onStateChange('timer', 'inspection', function(event) {
    var loop = function() {
        var time = 15 - Math.floor((performance.now() - timer.currentState.info.startedTimeStamp)/1000)
        if(timer.currentState.subState === 'inspection') {
            if(time > 0) {
                document.getElementById('timer').innerHTML = time
                window.setTimeout(loop, 0.5)
            } else if (time > -2 && 0 >= time){
                document.getElementById('timer').innerHTML = '+2'
                window.setTimeout(loop, 1)
                timer.penalties.plusTwo = true
            } else {
                document.getElementById('timer').innerHTML = 'DNF'
                timer.penalties.plusTwo = false
                timer.penalties.DNF = true
            }
        }
    }
    window.setTimeout(loop, 1)
})

timer.onStateChange('timer', 'timing', function(event) {
    var loop = function() {
        var time = people.formatTime(Math.floor(performance.now() - timer.currentState.info.startedTimeStamp)/1000)
        if(timer.currentState.subState === 'timing') {
            document.getElementById('timer').innerHTML = time
            window.setTimeout(loop, 0.5)
        }
    }
    window.setTimeout(loop, 1)
})

//timer end
addEvent(window, 'keydown', function(event) {
    if(event.code === 'Space') {
        if(timer.currentState.subState === 'timing') {
            var time = Math.floor(performance.now() - timer.currentState.info.startedTimeStamp)/1000
            timer.spaceDoStart = false
            var oldState = timer.currentState
            timer.setState(timer.timerState.idle())
            
            document.getElementById('timer').innerHTML = people.formatTime(time)
            timer.saveTime(people.formatTime(time), timer.penalties)
            
            timer.reloadUI()
        }
    }
    // time ops
    if(timer.currentState.subState === 'idle') {
        if(event.code === 'Backspace') {
            timer.deleteLatestTime(timer.session)
        }
    }
})

document.addEventListener('contextmenu', event => event.preventDefault());